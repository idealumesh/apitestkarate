# REST API testing with Karate DSL

This project will show how to Automate REST-APIs of Aylien NEWS stories API - https://api.aylien.com/news/stories
Few sample test cases provided in BDD with Karate DSL 

This project includes scripts for CI pipeline integration with GITLAB.

## How to get project and use
Clone the project 
```shell
git clone https://gitlab.com/idealumesh/apitestkarate.git
cd apitestkarate
```
To run the project
```shell
mvn clean test
```
## A simple scenarios
The project comes with simple scenarios to test the https://api.aylien.com/news/stories
API references can be obtained from https://docs.aylien.com/newsapi/endpoints/#stories

Note : You need to register at https://newsapi.aylien.com/ and obtained API keys.


Sample feature files given blow. Other featues can be found under the folder src/test/java/aylien

Note : This project also contains other generic samples in other folder which can be activated through plugins through pom.xml

```Gherkin
# Feature to search for stories with varios keywords
Feature: Aylien API -  The News stories - Search with Keywords

  Background:
    # read the base json files to get the headers and parameters
    * def api_headers = read("app_info.json")
    * def base_params = read("base_params.json")
    # set base url for Aylien new API
    * def baseurl = 'https://api.aylien.com/news/'
    * configure afterScenario = function(){ karate.log('API Response : ', karate.pretty(response)) }
    
  @priority=1 @systest @regression
  Scenario Outline: Verify if stories are retrieved correctly when request made with keyword <keyword>

    Given url baseurl + 'stories'
    # add an additional parameter in base parameters for keyword to search
    And set base_params.title = <keyword>
    And params base_params
    And headers api_headers
    When method GET
    Then status 200
     And print 'Number of stories retrieved: ' , response.stories.length
     And match response.stories[*].keywords contains deep <keyword>

    # repeat test with different keywords
    Examples:
    |keyword|
    |'Covid'  |
    |'lockdown'|
    |'party'   |

```

GitLab
This project is integrated with simple easy to understand CI/CD pipeline through the yml file .gitlab-ci.yml

.gitlab-ci.yml

```yaml
image: maven:3-jdk-8
Clean:
  stage: build
  script:
    - mvn clean
Build _and_Test:
  stage: test
  script:
    - mvn test
  artifacts:
    paths:
      - target/cucumber-html-reports/
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
        - target/failsafe-reports/TEST-*.xml

```

To view the pipeline navigate to https://gitlab.com/idealumesh/apitestkarate/-/pipelines.

Reports
1. Cucumber reports can be viewed through artifacts
2. Navigate to below path once artifacts are loaded after pipeline run
   target/cucumber-html-reports/overview-features.html


![alt text](https://gitlab.com/idealumesh/apitestproject/-/blob/master/Screenshot_2021-05-18_at_6.59.17_AM.png) 



   

# Feature for check for new stories
Feature: Stories API - Is fresh stories available?
  Is the volume of them comparable with the volumes from the past?

  Background:
    # Javascript function for calling 'base.feature' which makes REST api calls to stories endpoint
    # untill stories size reaches to zero
    # you may prefer to read the javascript from a file instead of having it in-line
    * def StoriesAPI_iterative =
    """
    function(story_size)
     {
        fetched_stories = []
        while (true) {
          var stories = []
          var result = karate.call('00_storiesAPI.feature', delta_param);
            size = result.response.stories.length;
            stories = result.response.stories
            fetched_stories = fetched_stories + stories
            delta_param.cursor = result.response.next_page_cursor
            karate.log('Current stories size :',size);
            karate.log('So far fetched: ' , fetched_stories.length)
            if (size == story_size)
            {
              karate.log('condition satisfied, exiting');
              return;
            }
        }
     }
    """

  @Regression # Tag to include test in to regression pack
  Scenario: Compare last two 5 mints blocks of Stories for search text - Umesh Kandhalu
    # define place holder to store total stories
    * def fetched_stories = []

    # Get the number of stories last five minutes block
    Given def delta_param = { cursor: '*', start : 'Now', end : 'NOW-5MINUTES'}
      And def story_size = 0
    Then call StoriesAPI_iterative story_size
      And def SizeOfLastFiveMinutes = fetched_stories.length

    # Get the number of stories second last five minutes block
    Given def delta_param = { cursor: '*', start : 'NOW-5MINUTES', end : 'NOW-10MINUTES'}
      And def story_size = 0
    Then call StoriesAPI_iterative story_size
     And def SizeOfPreviousFiveMinutes = fetched_stories.length

    # Compare each block stories for volume variation.
    # if number of stories both blocks matches make this test pass
    # otherwise fail with stating new stories are there.
      * if (SizeOfPreviousFiveMinutes < SizeOfLastFiveMinutes) { karate.log('New Stories available')};
      Then match SizeOfPreviousFiveMinutes == SizeOfLastFiveMinutes

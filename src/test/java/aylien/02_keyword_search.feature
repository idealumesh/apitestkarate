# Feature to search for stories with varios keywords
Feature: Aylien API -  The News stories - Search with Keywords

  Background:
    # read the base json files to get the headers and parameters
    * def api_headers = read("app_info.json")
    * def base_params = read("base_params.json")
    # set base url for Aylien new API
    * def baseurl = 'https://api.aylien.com/news/'
    * configure afterScenario = function(){ karate.log('API Response : ', karate.pretty(response)) }


  @priority=1 @systest @regression
  Scenario Outline: Verify if stories are retrieved correctly when request made with keyword <keyword>

    Given url baseurl + 'stories'
    # add an additional parameter in base parameters for keyword to search
    And set base_params.title = <keyword>
    And params base_params
    And headers api_headers
    When method GET
    Then status 200
     And print 'Number of stories retrieved: ' , response.stories.length
     And match response.stories[*].keywords contains deep <keyword>

    # repeat test with different keywords
    Examples:
    |keyword|
    |'Covid'  |
    |'lockdown'|
    |'party'   |
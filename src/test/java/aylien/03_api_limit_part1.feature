Feature: The News API - Rate limits

  Background:
    * def api_headers = read("app_info.json")
    * def base_params = read("base_params.json")
    * def baseurl = 'https://api.aylien.com/news/'
    * def now = function(){ return java.lang.System.currentTimeMillis()}
    * def wait = function(pause){ java.lang.Thread.sleep(pause) }
    * def start_time = now()
    * configure afterScenario =
  """
   function()
   {
      if (typeof duration !== 'undefined')
        {karate.log('Duration of calls', karate.pretty(duration/1000), start_time, end_time)}
      else { karate.log('429 status not accrued', start_time , now())}
   }
  """
  @priority=1 @systest @regression
  Scenario: Aylien API - Verify if rate-limiting is working correctly for a trial user

  # First get the remaining hits for the time block before starting bulk hits to check ratelimit
    Given url baseurl + 'stories'
    And params base_params
    And headers api_headers
    And method GET
    And def hits_rem = get responseHeaders.x-ratelimit-hit-remaining
    And def count =  parseInt(hits_rem)

  # Using remaining hits start bulk hit, configure retry count with remaining hits
    Given url baseurl + 'stories'
    And params base_params
    And headers api_headers
    When configure retry = { count: '#(count)', interval: 0 }
    Then retry until responseStatus == 429
    And method GET
    And def end_time = now()
    And def duration =  end_time - start_time
    And assert duration <= 3600*1000

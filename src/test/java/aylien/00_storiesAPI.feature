@ignore #  This feature will be ignored as it a base feature and will be called by other features
Feature: Base Feature Stories API

Background:
  * def api_headers = read("app_info.json")
  * def base_params = read("base_params.json")
  * def baseurl = 'https://api.aylien.com/news/'

  @ignore
  Scenario: Base Scenario
  Given url baseurl + 'stories'
    And set base_params.published_at_start = start
    And set base_params.published_at_end = end
    And set base_params.per_page = "100"
    And set base_params.cursor = cursor
    When params base_params
    And headers api_headers
  Then method GET
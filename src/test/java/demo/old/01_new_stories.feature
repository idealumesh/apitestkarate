Feature: The News API - New Stories volume compare

  Background:
    * def api_headers = read("app_info.json")
    * def base_params = read("base_params.json")
    * def baseurl = 'https://api.aylien.com/news/'

  @priority=1 @systest @regression
  Scenario: Aylien API - Verify if there is any new stories and compare with previous set
    Given url baseurl + 'stories'
      And set base_params.published_at_start = "NOW-5MINUTES"
      And set base_params.published_at_end = "NOW"
      And set base_params.per_page = "100"
      And params base_params
      And headers api_headers
    When method GET
    Then status 200
      And def story_length_CURRENT = response.stories.length
      And print 'Number of stories: ', story_length_CURRENT
      And print 'next_page_cursor: ', response.next_page_cursor
      And print karate.pretty(response)

    Given url baseurl + 'stories'
    And set base_params.published_at_start = "NOW-10MINUTES"
    And set base_params.published_at_end = "NOW-5MINUTES"
    And set base_params.per_page = "100"
    And params base_params
    And headers api_headers
    When method GET
    Then status 200
    And def story_length_PREVIOUS = response.stories.length
    And print 'Number of stories: ', story_length_PREVIOUS
    And print 'next_page_cursor: ', response.next_page_cursor
    And print karate.pretty(response)
    And match story_length_PREVIOUS == story_length_CURRENT

